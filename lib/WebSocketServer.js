var util = require("util");
var http = require("http");
var WebSocket = require("./WebSocket");

function WebSocketServer() {
  var self = this;
  http.Server.call(this);

  self.on("upgrade", function(req, sock, head) {
    handshake(req.headers, sock);
    var url = require("url").parse(req.url, true);
    var websock = new WebSocket(sock, url);

    self.emit("websock_connect", websock);

    sock.on('data', function(chunk) {
      for (var i=0; i < chunk.length; i++) {
        websock.read(chunk[i]);
      }
    });
  });
}

function handshake(headers, sock) {
  var websock_key = headers['sec-websocket-key'];
  websock_key +=  '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
  sock.write('HTTP/1.1 101 Switching Protocols\r\n');
  sock.write("Upgrade: websocket\r\n");
  sock.write("Connection: Upgrade\r\n");
  sock.write("Sec-WebSocket-Accept: "+base64sha1(websock_key)+"\r\n\r\n");
}

function base64sha1(chunk) {
  var sha1 = require('crypto').createHash('sha1');
  sha1.update(chunk);
  return sha1.digest('base64');
};

util.inherits(WebSocketServer, http.Server);

module.exports = WebSocketServer;

if (require.main === module) {
  (function() {
    var srv = new WebSocketServer();
    srv.on("websock_connect", function(websock) {
      websock.on("text", function(text) {
        console.log(text);
      });
    });
   srv.listen(9989);
   console.log("websocket echo server bind to port 9989");
   console.log("  sock = new WebSocket('ws://localhost:9989');");
   console.log("  sock.send('hello world');");
  })();
}
