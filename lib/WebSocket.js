var util = require("util");
var events = require("events");

function identity() {
  return identity;
}

// events:
// * 'text': text frame come in
// " 'close': socket closed
function WebSocket(tcpsock, url) {
  var self = this;
  self.url = url;
  self.tcpsock = tcpsock;
  self.read = makereader(self);
}
util.inherits(WebSocket, events.EventEmitter);

WebSocket.prototype.send = function(opcode, payload) {
  if (opcode === 0x08) {
    // SHOULD not send any data after sending a close frame
    this.send = function() {};
    this.tcpsock.end(encode(opcode, payload));
    this.emit("close");
  } else {
    this.tcpsock.write(encode(opcode, payload));
  }
};

WebSocket.prototype.sendText = function(text) {
  this.send(0x01, text);
};

WebSocket.prototype.sendBinary = function(data) {
  this.send(0x02, data);
};

WebSocket.prototype.close = function() {
  this.send(0x08);
};

WebSocket.prototype.ping = function() {
  this.send(0x09);
};

WebSocket.prototype.pong = function() {
  this.send(0x0A);
};

function encode(opcode, payload) {
  var packet = [];
  var payload_length = 0;
  if (opcode === 0x01 && typeof(payload) === 'string') {
    payload = new Buffer(payload);
  }
  packet.push(opcode | 0x80);
  if (payload instanceof Buffer) {
    payload_length = payload.length;
  } else {
    payload = new Buffer('');
  }
  if (payload_length >= 0 && payload_length <= 125) {
    packet.push(payload_length);
  } else {
    packet.push(126);
    packet.push(payload_length >> 8);
    packet.push(payload_length & 0xff);
  }
  return Buffer.concat([new Buffer(packet), payload]);
}


function makereader(websock) {
  var action = make_fin_reader(websock);
  return function reader(byte) {
    action = action(byte);
  };
}

// utility functions for reading bytes
// first byte
function make_fin_reader(state) {
  return function fin_reader(byte) {
    state.opcode = byte & 0x0f;
    state.fin = !!(byte & 0x80);
    return make_payload_reader(state);
  };
}

function make_payload_reader(state) {
  return function payload_reader(byte) {
    state.mask = byte & 0x80;
    byte &= 0x7f;

    if (byte >= 0 && byte <= 125) {
      state.length = byte;
      return make_body_reader(state);
    } else if (byte === 126) {
      return make_payload_reader_2(state);
    } else if (byte === 127) {
      return make_payload_reader_8(state);
    }
  };
}


function make_payload_reader_2(state) {
  var buffer = [];
  function payload_reader_2(byte) {
    buffer.push(byte);
    if (buffer.length === 2) {
      state.length = decode_network_byte_order_buffer(buffer);
      return make_body_reader(state);
    } else {
      return payload_reader_2;
    }
  }
  return payload_reader_2;
}

function encode_network_byte_order_integer(int, len) {

}

function decode_network_byte_order_buffer(buf) {
  var order;
  var sum = 0;
  for (var i=0; i < buf.length; i++) {
    order = buf.length - i - 1;
    if (buf[i] !== 0) {
      sum += buf[i] * Math.pow(2, order*8);
    }
  }
  return sum;
}

function make_payload_reader_8(state) {
  var buffer = [];
  function payload_reader_8(byte) {
    buffer.push(byte);
    if (buffer.length === 8) {
      state.length = network_byte_order(buffer);
      return make_body_reader(state);
    } else {
      return payload_reader_8;
    }
  }
  return payload_reader_8;
}


function make_body_reader(state) {
  if (state.mask) {
    return make_masked_body_reader(state);
  } else {
    // terminate connection because websocket server SHOULD not accepted
    // unmasked data
    state.tcpsock.destory();
  }
}

function packet_complete(state, buffer) {
  buffer = new Buffer(buffer);
  var stream;
  var stage;
  if (state.opcode === 0) { // continuing fragmented packet
    state.stage.push(buffer);
    state.stage.is_finalized = state.fin;
    state.frameStream.emit("readable");
  } else {
    stage = [];
    var frameStream = new require("stream").Readable();
    if (state.opcode === 0x01 && state.opcode === 0x02) {
      state.stage = stage;
      state.frameStream = frameStream;
    }
    frameStream._read = function(size) {
      if (stage.length === 0) {
        this.push(new Buffer(''));
      } else {
        while (stage.length > 0) {
          this.push(stage.shift());
        }
      }
      if (stage.is_finalized) {
        this.push(null);
      }
    };
    if (state.opcode === 0x02) {
      state.emit("binary", frameStream);
    } else if (state.opcode === 0x01) {
      (function(buf) {
        frameStream.on("data", function(data) {
          buf.push(data);
        });
        frameStream.on("end", function() {
          state.emit("text", Buffer.concat(buf).toString("utf8"));
        });
      })([]);
    } else if (state.opcode === 0x08) { // close
      state.close();
    } else if (state.opcode === 0x09) { // ping
      state.pong();
    }

    stage.push(buffer);
    stage.is_finalized = state.fin;
    frameStream.emit("readable");
  }

  return make_fin_reader(state);
}

function make_masked_body_reader(state) {
  var masks = [];
  var buffer = [];
  function maskkey_reader(byte) {
    masks.push(byte);
    if (masks.length === 4) {
      state.masks = masks;
      return body_reader;
    } else {
      return maskkey_reader;
    }
  }

  function body_reader(byte) {
    var text;

    var offset = buffer.length;
    byte ^= state.masks[offset % 4];
    buffer.push(byte);
    if (buffer.length === state.length) {
      return packet_complete(state, buffer);
    } else {
      return body_reader;
    }
  }
  return maskkey_reader;
}

module.exports = WebSocket;
