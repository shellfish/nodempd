var util = require("util");
var MPDCommander = require("./MPDCommander");
var _ = require("underscore");

// events:
// * change: mpd server changed state
function MPDMonitor () {
  var self = this;
  MPDCommander.call(this);

  self.is_idling = false;
  self.is_querying = false;

  self.on("close", function() {
    self._issue = function() {};  // no command is issues after close
    console.log("connection closed");
  });

  self.on("change", function(changes) {
    console.log("=== STATUS CHANGED ===");
    console.log(changes);
  });

  function idle_callback(subsystems) {
    function report(changes) {
      if (Object.keys(changes).length > 0) {
        self.emit("change", changes);
      }
    }

    self.STATUS(function(status) {
      var changes = {};

      function update(attr) {
        if (self.status[attr] !== status[attr]) {
          changes[attr] = status[attr];
        }
      }

      subsystems = subsystems.reduce(function(output, input) {
        output[input] = true;
        return output;
      }, {});


      if (subsystems.player) {
        ["state", "songid", 'elapsed'].forEach(update);
      }

      if (subsystems.playlist) {
        update("playlistlength");
        self.PLCHANGES(function(plchanges) {
          if (plchanges.length > 0) {
            changes.plchanges = plchanges;
          }
          self.status = status;
          report(changes);
        }, self.status.playlist);
      } else {
        self.status = status;
        report(changes);
      }
    });
  }
  self.setup_idle = function() {
    self.IDLE(idle_callback);
  };

  self.on("accepted", function() {
    self.STATUS(function(status) {
      self.status = status;
      self.emit("init_status", status);
    });
  });
}

util.inherits(MPDMonitor, MPDCommander);

var transformers = {};

transformers.idle = (function() {
  var regexp = /^changed: (.+)$/;
  return function(frame) {
    var data;
    if (frame.length === 0) {
      return;
    } else {
      data = [];
      frame.forEach(function(line) {
        var result = regexp.exec(line);
        data.push(result[1]);
      });
    }
    return data;
  }
})();

var keyvalue_transformer  = (function() {
  var regexp = /^(.+): (.+)$/;
  return function(lines) {
    var data = {};
    lines.forEach(function(line) {
      var result = regexp.exec(line);
      data[result[1]] = result[2];
    });
    return data;
  };
})();

transformers.playlistinfo = function(frame) {
  if (frame.length === 0) {
    return [];
  }
  var songs = [[]];
  var song = songs[0];
  var count = 0;
  var playlist = [];
  var line;
  for (var i=0; i < frame.length; i++) {
    line = frame[i];
    song.push(line);
    if (i !== frame.length - 1 &&
        line[0] === 'I' && line[1] === 'd' && line[2] === ':') {
      song = [];
      songs.push(song);
    }
  }
  var regexp = /^(.+): (.+)$/;
  songs.forEach(function(song) {
    playlist.push(keyvalue_transformer(song));
  });
  return playlist;
};

transformers.plchanges = transformers.playlistinfo;

transformers.status = function(d) {
  var x = keyvalue_transformer(d);
  if (x.length !== 0) {
    return x;
  }
};

function make_transformed_callback(transformer, callback) {
  return function newcb(frame) {
    var data = transformer(frame);
    if (data !== undefined) {
      return callback(data);
    }
  };
}

MPDMonitor.prototype._issue = function(cmd, cb) {
  var origcb;
  var self = this;
  var extra;
  var is_nested_querying = false;

  if (transformers.hasOwnProperty(cmd)) {
    cb = make_transformed_callback(transformers[cmd], cb);
  }
  origcb = cb;

  if (cmd === "idle") { // idle waiting
    this.is_idling = true;
    cb = function() {
      self.is_idling = false;
      origcb.apply(this, arguments);
      if (!self.is_querying) {
        self.setup_idle();
      }
    };
  } else if (cmd === "noidle") { // normal commands
    MPDCommander.prototype._issue.call(this, cmd);
    return;
  } else {
    if (self.is_querying) {
      is_nested_querying = true;
    } else {
      self.is_querying = true;
    }
    if (self.is_idling) {
      self.NOIDLE();
    }
    cb = function() {
      if (typeof origcb === 'function') {
        origcb.apply(this, arguments);
      }
      if (!is_nested_querying) {
        self.setup_idle();
        self.is_querying = false;
      }
    };
  }

  extra = Array.prototype.slice.call(arguments, 2, arguments.length);
  MPDCommander.prototype._issue.apply(this, [cmd, cb].concat(extra));
};


module.exports = MPDMonitor;

if (require.main === module) {
  (function() {
    var x = new MPDMonitor();
    x.connect(6600, 'localhost');
  })();
}
