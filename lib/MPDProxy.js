var events = require("events");
var net = require("net");
var util = require("util");

function make_line_reader(line_consumer) {
  var buffer = [];
  function parser(char) {
    if (char !== '\n') {
      buffer.push(char);
      return parser;
    } else {
      return line_consumer(buffer.join(""));
    }
  }
  return parser;
}

function make_frame_reader(state) {
  var lines = [[]];
  function parser(char) {
    if (char !== '\n') {
      lines[lines.length - 1].push(char);
    } else {
      lines[lines.length - 1] = lines[lines.length - 1].join("");
      if (lines[lines.length - 1] === "OK") {
        // end of frame
        lines.pop();
        state.emit("frame", lines);
        lines = [[]];
      } else {
        // frame continues
        lines[lines.length] = [];
      }
    }
    return parser;
  }
  return parser;
}

function make_greeting_reader(state) {
  var buffer = [];
  function parser(char) {
    buffer.push(char);
    if (buffer.length === 7) {
      if (buffer.join("") === "OK MPD ") {
        return make_line_reader(function(version_number) {
          state.version = version_number;
          state.emit("accepted");
          return make_frame_reader(state);
        });
      } else {
        throw new Error("Invalid protocal greeting");
      }
    } else {
      return parser;
    }
  }
  return parser;
}

// events:
// 'accepted': after welcome message is received
// 'close': after mpd socket is closed
// 'frame': a frame of data is received, array of trimmed lines
//
// methods:
// connect(port, host)
// close()
function MPDProxy() {
  var self = this;
  var parser;
  var sock;

  function make_parser() {
    var state = self;
    return make_greeting_reader(state);
  }

  parser = make_parser();
  sock = new net.Socket({readable: true, writable: true});
  sock.setEncoding('utf8');
  sock.on('data', function(data) {
    var t;
    for (var i=0; i < data.length; i++) {
      t = parser(data[i]);
      parser = t;
    }
  });

  sock.on("connect", function() {
    self.send = function send(msg) {
      console.log("\033[31;1mCMD: " + msg.trim() + "\033[0m");
      sock.write(msg);
    };

    self.close = function close() {
      sock.write("close\n");
    };

    sock.on("close", function() {
      self.emit("close");
    });
  });

  events.EventEmitter.call(this);
  self.connect = function connect(port, host) {
    sock.connect(port, host);
  };
}

util.inherits(MPDProxy, events.EventEmitter);

module.exports = MPDProxy;
