// mix WebSocketServer and MPDMonitor
//
var WebSocketServer = require("./WebSocketServer");
var MPDMonitor = require("./MPDMonitor");
var util = require("util");

function MPDServer() {
  WebSocketServer.call(this);
  this.on("websock_connect", function(websock) {
    var host = websock.url.query.host || "localhost";
    var port = websock.url.query.port || 6600;
    var monitor = new MPDMonitor();
    monitor.connect(port, host);
    monitor.on("init_status", function(status) {
      websock.sendText(JSON.stringify(status, null, 3));
    });
    monitor.on("change", function(changes) {
      websock.sendText(JSON.stringify(changes, null, 3));
    });
    websock.on("close", function() {
      if (monitor.close) {
        monitor.close();
      }
    });
  });
}

util.inherits(MPDServer, WebSocketServer);

if (require.main === module) {
  (function() {

    function page_action() {
      var sock = new WebSocket("ws://localhost:9999");
      sock.onmessage = function(event) {
        var data = event.data;
        console.log(data);
      }
    }

    var srv = new MPDServer();
    srv.on('request', function(req, res) {
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write('<html><body>Open your javascript console<script>');
      res.write(page_action.toString());
      res.write("page_action();");
      res.end("</script></body></html");
    });
    srv.listen(9999);
    console.log("listening on 9999");
  })();
}
