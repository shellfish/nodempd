var util = require("util");
var MPDProxy = require('./MPDProxy');

function MPDCommander() {
  var self = this;
  MPDProxy.call(this);
  this.cbqueue = [];

  self.on("frame", function(frame) {
    self.cbqueue.shift()(frame);
  });
}

util.inherits(MPDCommander, MPDProxy);

MPDCommander.prototype._issue = function(cmd, cb) {
  var extra = Array.prototype.slice.call(arguments, 2, arguments.length);
  this.send(cmd + ' ' + extra.join(' ') + "\n");
  if (typeof cb === 'function') {
    this.cbqueue.push(cb);
  }
};

var auto = 'auto';
var commands = [
  ["idle", true],
  ["noidle", false],
  ["status", true],
  ["playlistinfo", true],
  ['plchanges', true],
  ["play", auto],
  ["pause", auto],
  ["pause", auto],
  ["currentsong", true],
  ["next", auto]
];

MPDCommander.prototype.addCommand = function(cmd, hasCb) {
  var method = cmd.toUpperCase();
  if (hasCb) {
    if (hasCb !== auto) {
      this[method] = function(cb) {
        this._issue(cmd, cb,
               Array.prototype.slice.call(arguments, 1, arguments.length));
      };
    } else {
      this[method] = function() {
        this._issue(cmd, function auto() {},
                    Array.prototype.slice.call(arguments));
      };
    }
  } else {
    this[method] = function() {
      this._issue(cmd, null, Array.prototype.slice.call(arguments));
    };
  }
};


for (var c in commands) {
  MPDCommander.prototype.addCommand.apply(MPDCommander.prototype,
                                          commands[c]);
}

module.exports = MPDCommander;
